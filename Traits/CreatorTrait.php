<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 26/10/2021
 * Time: 10:11 am
 */

namespace app\Traits;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;


trait CreatorTrait
{
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
//                'value' =>function(){
//                   return time();
//                },
            ],
        ];
    }


}
