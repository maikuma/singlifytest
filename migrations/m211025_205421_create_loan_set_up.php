<?php

use yii\db\Migration;

/**
 * Class m211025_205421_create_loan_set_up
 */
class m211025_205421_create_loan_set_up extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        //Main Users
        $this->createTable('{{%loan_set_up}}', [
            'id' => $this->primaryKey(),
            'loan_name' => $this->string()->notNull()->unique()->comment('Name of the Loan'),
            'loan_interest' => $this->integer()->comment('The interest that will apply on the loan'),
            'loan_processing_fee' => $this->integer()->notNull()->comment('The Process fee that will apply on the loan'),
            'loan_administrative_fee' => $this->integer()->notNull()->comment('The Administrative fee that will apply on the loan'),
            'loan_risk_fee' => $this->integer()->notNull()->comment('The Risk fee that will apply on the loan'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'deleted_by' => $this->integer(),
        ], $tableOptions);

        // creates index for column `created_by` in loan_set_up
        $this->createIndex(
            '{{%idx-loan_set_up-created_by}}',
            '{{%loan_set_up}}',
            'created_by'
        );

        // add foreign key for table `{{%loan_set_up}}`
        $this->addForeignKey(
            '{{%fk-loan_set_up-created_by}}',
            '{{%loan_set_up}}',
            'created_by',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by` in loan_set_up
        $this->createIndex(
            '{{%idx-loan_set_up-updated_by}}',
            '{{%loan_set_up}}',
            'updated_by'
        );

        // add foreign key for table `{{%loan_set_up}}`
        $this->addForeignKey(
            '{{%fk-loan_set_up-updated_by}}',
            '{{%loan_set_up}}',
            'updated_by',
            '{{%users}}',
            'id',
            'no action'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%loan_set_up}}`
        $this->dropForeignKey(
            '{{%fk-loan_set_up-created_by}}',
            '{{%loan_set_up}}'
        );

        // drops index for column `created_by` in loan_set_up
        $this->dropIndex(
            '{{%idx-loan_set_up-created_by}}',
            '{{%loan_set_up}}'
        );

        // drops foreign key for table `{{%loan_set_up}}`
        $this->dropForeignKey(
            '{{%fk-loan_set_up-updated_by}}',
            '{{%loan_set_up}}'
        );

        // drops index for column `updated_by` in loan_set_up
        $this->dropIndex(
            '{{%idx-loan_set_up-updated_by}}',
            '{{%loan_set_up}}'
        );

        //Dropping the loan_set_up table
        $this->dropTable('{{%loan_set_up}}');

    }
}
