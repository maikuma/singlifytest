<?php

use yii\db\Migration;

/**
 * Class m211025_210022_create_customer
 */
class m211025_210022_create_customer extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Setting user account Banks accounts details
        $this->createTable('{{%customers}}', [
            'id' => $this->primaryKey(),
            'customers_name' => $this->string()->notNull()->comment('Customer Name'),
            'customers_email' => $this->string()->notNull()->comment('Customer Email Address'),
            'customers_phone' => $this->string()->notNull()->comment('Customer Phone number'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'deleted_by' => $this->integer(),
        ], $tableOptions);

        // creates index for column `created_by` in customers
        $this->createIndex(
            '{{%idx-customers-created_by}}',
            '{{%customers}}',
            'created_by'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-customers-created_by}}',
            '{{%customers}}',
            'created_by',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by` in customers
        $this->createIndex(
            '{{%idx-customers-updated_by}}',
            '{{%customers}}',
            'updated_by'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-customers-updated_by}}',
            '{{%customers}}',
            'updated_by',
            '{{%users}}',
            'id',
            'no action'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-customers-created_by}}',
            '{{%customers}}'
        );

        // drops index for column `created_by` in customers
        $this->dropIndex(
            '{{%idx-customers-created_by}}',
            '{{%customers}}'
        );

        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-customers-updated_by}}',
            '{{%customers}}'
        );

        // drops index for column `updated_by` in customers
        $this->dropIndex(
            '{{%idx-customers-updated_by}}',
            '{{%customers}}'
        );

        //Dropping the customers table
        $this->dropTable('{{%customers}}');


    }
}
