<?php

use yii\db\Migration;

/**
 * Class m211026_054955_create_loan_payment_months
 */
class m211026_054955_create_loan_payment_months extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Setting Customer Loan application details
        $this->createTable('{{%loan_payment_months}}', [
            'id' => $this->primaryKey(),
            'application_id' => $this->integer()->notNull()->comment('The Loan been taken'),
            'month_1'=>$this->boolean()->defaultValue(false),
            'month_2'=>$this->boolean()->defaultValue(false),
            'month_3'=>$this->boolean()->defaultValue(false),
            'month_4'=>$this->boolean()->defaultValue(false),
            'month_5'=>$this->boolean()->defaultValue(false),
            'month_6'=>$this->boolean()->defaultValue(false),
            'month_7'=>$this->boolean()->defaultValue(false),
            'month_8'=>$this->boolean()->defaultValue(false),
            'month_9'=>$this->boolean()->defaultValue(false),
            'month_10'=>$this->boolean()->defaultValue(false),
            'month_11'=>$this->boolean()->defaultValue(false),
            'month_12'=>$this->boolean()->defaultValue(false),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'deleted_by' => $this->integer(),
        ], $tableOptions);

        // creates index for column `application_id` in loan_payment_months
        $this->createIndex(
            '{{%idx-loan_payment_months-application_id}}',
            '{{%loan_payment_months}}',
            'application_id'
        );

        // add foreign key for table `{{%loan_payment_months}}`
        $this->addForeignKey(
            '{{%fk-loan_payment_months-application_id}}',
            '{{%loan_payment_months}}',
            'application_id',
            '{{%loan_application}}',
            'id',
            'CASCADE'
        );

        // creates index for column `created_by` in loan_payment_months
        $this->createIndex(
            '{{%idx-loan_payment_months-created_by}}',
            '{{%loan_payment_months}}',
            'created_by'
        );

        // add foreign key for table `{{%loan_payment_months}}`
        $this->addForeignKey(
            '{{%fk-loan_payment_months-created_by}}',
            '{{%loan_payment_months}}',
            'created_by',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by` in loan_payment_months
        $this->createIndex(
            '{{%idx-loan_payment_months-updated_by}}',
            '{{%loan_payment_months}}',
            'updated_by'
        );

        // add foreign key for table `{{%loan_payment_months}}`
        $this->addForeignKey(
            '{{%fk-loan_payment_months-updated_by}}',
            '{{%loan_payment_months}}',
            'updated_by',
            '{{%users}}',
            'id',
            'no action'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%loan_payment_months}}`
        $this->dropForeignKey(
            '{{%fk-loan_payment_months-application_id}}',
            '{{%loan_payment_months}}'
        );

        // drops index for column `application_id` in loan_payment_months
        $this->dropIndex(
            '{{%idx-loan_payment_months-application_id}}',
            '{{%loan_payment_months}}'
        );

        // drops foreign key for table `{{%loan_payment_months}}`
        $this->dropForeignKey(
            '{{%fk-loan_payment_months-created_by}}',
            '{{%loan_payment_months}}'
        );

        // drops index for column `created_by` in loan_payment_months
        $this->dropIndex(
            '{{%idx-loan_payment_months-created_by}}',
            '{{%loan_payment_months}}'
        );

        // drops foreign key for table `{{%loan_payment_months}}`
        $this->dropForeignKey(
            '{{%fk-loan_payment_months-updated_by}}',
            '{{%loan_payment_months}}'
        );

        // drops index for column `updated_by` in loan_payment_months
        $this->dropIndex(
            '{{%idx-loan_payment_months-updated_by}}',
            '{{%loan_payment_months}}'
        );

        //Dropping the loan_payment_months table
        $this->dropTable('{{%loan_payment_months}}');


    }

}
