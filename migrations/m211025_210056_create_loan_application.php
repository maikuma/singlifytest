<?php

use yii\db\Migration;

/**
 * Class m211025_210056_create_loan_application
 */
class m211025_210056_create_loan_application extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Setting Customer Loan application details
        $this->createTable('{{%loan_application}}', [
            'id' => $this->primaryKey(),
            'loan_id' => $this->integer()->notNull()->comment('The Loan been taken'),
            'customer_id' => $this->integer()->notNull()->comment('Customer Applying for a Loan'),
            'application_no' => $this->string()->notNull()->unique()->comment('Application Number'),
            'loan_amount' => $this->integer()->notNull()->comment('Tha Amount been taken '),
            'duration' => $this->integer()->notNull()->comment('The time in months'),
            'loan_interest' => $this->integer()->comment('The interest that will apply on the loan'),
            'loan_processing_fee' => $this->integer()->notNull()->comment('The Process fee that will apply on the loan'),
            'loan_administrative_fee' => $this->integer()->notNull()->comment('The Administrative fee that will apply on the loan'),
            'loan_risk_fee' => $this->integer()->notNull()->comment('The Risk fee that will apply on the loan'),
            'disbursement_date'=> $this->date()->notNull()->comment('the day of getting the loan'),
            'monthly_due_date'=> $this->integer()->notNull()->comment('The payment day of the loan every month'),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'deleted_at' => $this->timestamp(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'deleted_by' => $this->integer(),
        ], $tableOptions);

        // creates index for column `loan_id` in loan_application
        $this->createIndex(
            '{{%idx-loan_application-loan_id}}',
            '{{%loan_application}}',
            'loan_id'
        );

        // add foreign key for table `{{%loan_application}}`
        $this->addForeignKey(
            '{{%fk-loan_application-loan_id}}',
            '{{%loan_application}}',
            'loan_id',
            '{{%loan_set_up}}',
            'id',
            'CASCADE'
        );

        // creates index for column `customer_id` in loan_application
        $this->createIndex(
            '{{%idx-loan_application-customer_id}}',
            '{{%loan_application}}',
            'customer_id'
        );

        // add foreign key for table `{{%loan_application}}`
        $this->addForeignKey(
            '{{%fk-loan_application-customer_id}}',
            '{{%loan_application}}',
            'customer_id',
            '{{%customers}}',
            'id',
            'CASCADE'
        );


        // creates index for column `created_by` in loan_application
        $this->createIndex(
            '{{%idx-loan_application-created_by}}',
            '{{%loan_application}}',
            'created_by'
        );

        // add foreign key for table `{{%loan_application}}`
        $this->addForeignKey(
            '{{%fk-loan_application-created_by}}',
            '{{%loan_application}}',
            'created_by',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        // creates index for column `updated_by` in loan_application
        $this->createIndex(
            '{{%idx-loan_application-updated_by}}',
            '{{%loan_application}}',
            'updated_by'
        );

        // add foreign key for table `{{%loan_application}}`
        $this->addForeignKey(
            '{{%fk-loan_application-updated_by}}',
            '{{%loan_application}}',
            'updated_by',
            '{{%users}}',
            'id',
            'no action'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%loan_application}}`
        $this->dropForeignKey(
            '{{%fk-loan_application-loan_id}}',
            '{{%loan_application}}'
        );

        // drops index for column `loan_id` in loan_application
        $this->dropIndex(
            '{{%idx-loan_application-loan_id}}',
            '{{%loan_application}}'
        );

        // drops foreign key for table `{{%loan_application}}`
        $this->dropForeignKey(
            '{{%fk-loan_application-customer_id}}',
            '{{%loan_application}}'
        );

        // drops index for column `customer_id` in loan_application
        $this->dropIndex(
            '{{%idx-loan_application-customer_id}}',
            '{{%loan_application}}'
        );

        // drops foreign key for table `{{%loan_application}}`
        $this->dropForeignKey(
            '{{%fk-loan_application-created_by}}',
            '{{%loan_application}}'
        );

        // drops index for column `created_by` in loan_application
        $this->dropIndex(
            '{{%idx-loan_application-created_by}}',
            '{{%loan_application}}'
        );

        // drops foreign key for table `{{%loan_application}}`
        $this->dropForeignKey(
            '{{%fk-loan_application-updated_by}}',
            '{{%loan_application}}'
        );

        // drops index for column `updated_by` in loan_application
        $this->dropIndex(
            '{{%idx-loan_application-updated_by}}',
            '{{%loan_application}}'
        );

        //Dropping the loan_application table
        $this->dropTable('{{%loan_application}}');


    }
}
