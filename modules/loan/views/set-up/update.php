<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\loan\models\LoanSetUp */

$this->title = 'Update Loan Set Up: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Loan Set Ups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<!-- Form grid -->
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-30">
            <div class="card-body">
                <div class="card-header">
                    <h5 class="card-title"><?= Html::encode($this->title) ?></h5>
                </div>
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>


            </div>
        </div>
    </div>
</div>
<!-- End Form grid -->
