<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\loan\models\LoanSetUp */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="form-row">
    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_name', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_name']
        ])->textInput()->input('text', ['placeholder' => "Enter Loan Name *"]); ?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_interest', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_interest']
        ])->textInput()->input('number', ['placeholder' => "Enter Interest % *"]); ?>
    </div>

    <div class="form-group col-md-6">

        <?= $form->field($model, 'loan_processing_fee', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_processing_fee']
        ])->textInput()->input('number', ['placeholder' => "Enter Loan Processing fee % *"]); ?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_administrative_fee', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_administrative_fee']
        ])->textInput()->input('number', ['placeholder' => "Enter Loan Admin Fee % *"]); ?>
    </div>

    <div class="form-group col-md-6">

        <?= $form->field($model, 'loan_risk_fee', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_risk_fee']
        ])->textInput()->input('number', ['placeholder' => "Enter Loan Risk Fee % *"]); ?>
    </div>

</div>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
