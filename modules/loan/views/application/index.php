<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\loan\models\LoanApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loan Applications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="card mb-30">
            <div class="card-body">
                <div class="card-header">
                    <!--					<a href="#" class="btn btn-primary float-right">View all</a>-->
                    <?= Html::a('Add', ['create'], ['class' => 'btn btn-primary float-right']) ?>
                    <h5 class="card-title">
                        <?= Html::encode($this->title) ?>
                    </h5>
                </div>

                <div class="table-responsive">
                    <?php Pjax::begin(); ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-hover text-vertical-middle mb-0',
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
//                            'id',
//                            'loan_id',
                            [
                                'attribute' => 'loan_id',
                                'value' => 'loan.loan_name',
                            ],
                            'application_no',
                            [
                                'attribute' => 'customer_id',
                                'value' => 'customer.customers_name',
                            ],
//                            'customer_id',
                            'loan_amount',
                            'duration',
                            //'loan_interest',
                            //'loan_processing_fee',
                            //'loan_administrative_fee',
                            //'loan_risk_fee',
                            //'disbursement_date',
                            //'monthly_due_date',
                            //'status',
                            //'created_at',
                            //'updated_at',
                            //'deleted_at',
                            //'created_by',
                            //'updated_by',
                            //'deleted_by',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
