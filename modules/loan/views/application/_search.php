<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\loan\models\LoanApplicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-application-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'loan_id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'loan_amount') ?>

    <?= $form->field($model, 'duration') ?>

    <?php // echo $form->field($model, 'loan_interest') ?>

    <?php // echo $form->field($model, 'loan_processing_fee') ?>

    <?php // echo $form->field($model, 'loan_administrative_fee') ?>

    <?php // echo $form->field($model, 'loan_risk_fee') ?>

    <?php // echo $form->field($model, 'disbursement_date') ?>

    <?php // echo $form->field($model, 'monthly_due_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
