<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\loan\models\LoanApplication */
/* @var $modelPayment app\modules\loan\models\LoanPaymentMonths */

$this->title = 'Create Loan Application';
$this->params['breadcrumbs'][] = ['label' => 'Loan Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Form grid -->
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-30">
            <div class="card-body">
                <div class="card-header">
                    <h5 class="card-title"><?= Html::encode($this->title) ?></h5>
                </div>
                <?= $this->render('_form', [
                    //'model' => $model,
                    'model' => $model,
                    'modelPayment' => $modelPayment,
                ]) ?>


            </div>
        </div>
    </div>
</div>
<!-- End Form grid -->
