<?php

use app\modules\loan\models\Customers;
use app\modules\loan\models\LoanSetUp;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\loan\models\LoanApplication */
/* @var $modelPayment app\modules\loan\models\LoanPaymentMonths */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="form-row">
    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_id')
            ->dropDownList(
                ArrayHelper::map(LoanSetUp::find()->asArray()->all(), 'id', 'loan_name')
                , array('prompt' => 'Select Loan Product *')
            );
        ?>

    </div>
    <div class="form-group col-md-6">

        <?= $form->field($model, 'customer_id')
            ->dropDownList(
                ArrayHelper::map(Customers::find()->asArray()->all(), 'id', 'customers_name')
                , array('prompt' => 'Select Customer *')
            );
        ?>
    </div>

    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_amount', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_amount']
        ])->textInput()->input('number', ['placeholder' => "Enter Figure"]); ?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'duration', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'duration']
        ])->textInput()->input('number', ['placeholder' => "Enter Loan Duration in Months"]); ?>
    </div>
    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_interest', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_interest']
        ])->textInput()->input('number', ['placeholder' => "Loan Interest"]); ?>
    </div>

    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_administrative_fee', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_administrative_fee']
        ])->textInput()->input('number', ['placeholder' => "Enter Loan Admin Fee % *"]); ?>
    </div>

    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_processing_fee', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_processing_fee']
        ])->textInput()->input('number', ['placeholder' => "Enter Loan Process Fee"]); ?>
    </div>

    <div class="form-group col-md-6">
        <?= $form->field($model, 'loan_risk_fee', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_risk_fee']
        ])->textInput()->input('number', ['placeholder' => "Enter Loan Risk fee % *"]); ?>
    </div>

    <div class="form-group col-md-6">

        <?= $form->field($model, 'disbursement_date', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_risk_fee']
        ])->textInput()->input('date', ['placeholder' => "Enter Due Date*", 'format' => 'yyyy-M-dd']); ?>

    </div>

    <div class="form-group col-md-6">
        <?= $form->field($model, 'monthly_due_date', [
            'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'loan_risk_fee']
        ])->textInput()->input('number', ['placeholder' => "Enter Due Date*"]); ?>
    </div>


</div>

<p class="note">Select <span class="required">*</span> Payment Months.</p>
<div class="table-responsive">
    <table class="table">
        <thead>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th>6</th>
        <th>7</th>
        <th>8</th>
        <th>9</th>
        <th>10</th>
        <th>11</th>
        <th>12</th>
        </thead>
        <tbody>
        <tr>
            <td>
                Months
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_1')->checkbox()->label(false); ?>
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_2')->checkbox()->label(false); ?>

            </td>
            <td>
                <?= $form->field($modelPayment, 'month_3')->checkbox()->label(false); ?>

            </td>
            <td>
                <?= $form->field($modelPayment, 'month_4')->checkbox()->label(false); ?>

            </td>
            <td>
                <?= $form->field($modelPayment, 'month_5')->checkbox()->label(false); ?>
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_6')->checkbox()->label(false); ?>

            </td>
            <td>
                <?= $form->field($modelPayment, 'month_7')->checkbox()->label(false); ?>
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_8')->checkbox()->label(false); ?>
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_9')->checkbox()->label(false); ?>
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_10')->checkbox()->label(false); ?>
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_11')->checkbox()->label(false); ?>
            </td>
            <td>
                <?= $form->field($modelPayment, 'month_12')->checkbox()->label(false); ?>
            </td>

        </tr>
        </tbody>

    </table>
</div>
<div class="form-row">

</div>


<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
