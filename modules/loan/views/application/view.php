<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\loan\models\LoanApplication */

$this->title = $model->application_no;
$this->params['breadcrumbs'][] = ['label' => 'Loan Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-application-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-30">
            <div class="card-body todo-list">
                <div class="card-header">
                    <h5 class="card-title"><?= Html::encode($this->title) ?></h5>
                </div>

                <div class="row">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id',
//            'loan_id',
                            'loan.loan_name',
                            'application_no',
                            //'customer_id',
                            'customer.customers_name',
                            'loan_amount',
                            'duration',
                            'loan_interest',
                            'loan_processing_fee',
                            'loan_administrative_fee',
                            'loan_risk_fee',
                            'disbursement_date',
                            'monthly_due_date',

                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
