<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\loan\models\Customers */

$this->title = 'Create Customers';
$this->params['breadcrumbs'][] = ['label' => 'Customers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Form grid -->
<div class="row">
    <div class="col-xl-12">
        <div class="card mb-30">
            <div class="card-body">
                <div class="card-header">
                    <h5 class="card-title"><?= Html::encode($this->title) ?></h5>
                </div>
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>


            </div>
        </div>
    </div>
</div>
<!-- End Form grid -->
