<?php

namespace app\modules\loan\controllers;

//use app\models\Model;
use app\modules\loan\models\LoanApplication;
use app\modules\loan\models\LoanApplicationSearch;
use app\modules\loan\models\LoanPaymentMonths;
use Yii;

//use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

//use yii\web\Response;
//use yii\widgets\ActiveForm;

/**
 * ApplicationController implements the CRUD actions for LoanApplication model.
 */
class ApplicationController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all LoanApplication models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LoanApplicationSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LoanApplication model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LoanApplication model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LoanApplication();
        $modelPayment = new LoanPaymentMonths();

        if ($this->request->post()) {

            if ($model->load($this->request->post()) && $modelPayment->load($this->request->post())) {

                $model->application_no = uniqid();
                if ($model->save()) {

                    $modelPayment->application_id = $model->id;

                    $modelPayment->save();

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelPayment' => $modelPayment,
        ]);
    }

    /**
     * Updates an existing LoanApplication model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelPayment = $this->getLoanPayments($model->id,$model->status);
            //LoanPaymentMonths::findModel(['application_id' => $id]);

//        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
//
//            return $this->redirect(['view', 'id' => $model->id]);
//        }

        if ($this->request->post()) {

            if ($model->load($this->request->post()) && $modelPayment->load($this->request->post())) {

                if ($model->save()) {

                    $modelPayment->save();

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }
        return $this->render('update', [
            'model' => $model,
            'modelPayment' => $modelPayment,
        ]);

    }



    /**
     * Deletes an existing LoanApplication model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LoanApplication model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return LoanApplication the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LoanApplication::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function getLoanPayments($id,$status)
        {
          $model = LoanPaymentMonths::find()->where(['application_id' => $id])->Andwhere(['status' => $status])->all();
          return $model;
        }
}
