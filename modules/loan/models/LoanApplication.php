<?php

namespace app\modules\loan\models;

use app\models\User;
use app\Traits\CreatorTrait;
use Yii;

/**
 * This is the model class for table "loan_application".
 *
 * @property int $id
 * @property int $loan_id The Loan been taken
 * @property int $customer_id Customer Applying for a Loan
 * @property string $application_no Application Number
 * @property int $loan_amount Tha Amount been taken
 * @property int $duration The time in months
 * @property int|null $loan_interest The interest that will apply on the loan
 * @property int $loan_processing_fee The Process fee that will apply on the loan
 * @property int $loan_administrative_fee The Administrative fee that will apply on the loan
 * @property int $loan_risk_fee The Risk fee that will apply on the loan
 * @property string $disbursement_date the day of getting the loan
 * @property int $monthly_due_date The payment day of the loan every month
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int|null $deleted_by
 *
 * @property User $createdBy
 * @property Customers $customer
 * @property LoanSetUp $loan
 * @property LoanPaymentMonths[] $loanPaymentMonths
 * @property User $updatedBy
 */
class LoanApplication extends \yii\db\ActiveRecord
{
    use CreatorTrait;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_application';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['loan_id', 'customer_id', 'application_no', 'loan_amount', 'duration', 'loan_processing_fee', 'loan_administrative_fee', 'loan_risk_fee', 'disbursement_date', 'monthly_due_date'], 'required'],
            [['loan_id', 'customer_id', 'loan_amount', 'duration', 'loan_interest', 'loan_processing_fee', 'loan_administrative_fee', 'loan_risk_fee', 'monthly_due_date', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['disbursement_date', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by'], 'safe'],
            [['application_no'], 'string', 'max' => 255],
            [['application_no'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['loan_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanSetUp::className(), 'targetAttribute' => ['loan_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'loan_id' => 'Loan',
            'customer_id' => 'Customer',
            'application_no' => 'Application No',
            'loan_amount' => 'Loan Amount',
            'duration' => 'Duration',
            'loan_interest' => 'Loan Interest',
            'loan_processing_fee' => 'Loan Processing Fee',
            'loan_administrative_fee' => 'Loan Administrative Fee',
            'loan_risk_fee' => 'Loan Risk Fee',
            'disbursement_date' => 'Disbursement Date',
            'monthly_due_date' => 'Monthly Due Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * Gets query for [[Loan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLoan()
    {
        return $this->hasOne(LoanSetUp::className(), ['id' => 'loan_id']);
    }

    /**
     * Gets query for [[LoanPaymentMonths]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLoanPaymentMonths()
    {
        return $this->hasMany(LoanPaymentMonths::className(), ['application_id' => 'id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
