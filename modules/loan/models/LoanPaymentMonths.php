<?php

namespace app\modules\loan\models;

use app\models\User;
use app\Traits\CreatorTrait;
use Yii;

/**
 * This is the model class for table "loan_payment_months".
 *
 * @property int $id
 * @property int $application_id The Loan been taken
 * @property int|null $month_1
 * @property int|null $month_2
 * @property int|null $month_3
 * @property int|null $month_4
 * @property int|null $month_5
 * @property int|null $month_6
 * @property int|null $month_7
 * @property int|null $month_8
 * @property int|null $month_9
 * @property int|null $month_10
 * @property int|null $month_11
 * @property int|null $month_12
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int|null $deleted_by
 *
 * @property LoanApplication $application
 * @property User $createdBy
 * @property User $updatedBy
 */
class LoanPaymentMonths extends \yii\db\ActiveRecord
{
    use CreatorTrait;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_payment_months';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['application_id'], 'required'],
            [['application_id', 'month_1', 'month_2', 'month_3', 'month_4', 'month_5', 'month_6', 'month_7', 'month_8', 'month_9', 'month_10', 'month_11', 'month_12', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['application_id','created_at', 'updated_at', 'deleted_at','created_by', 'updated_by'], 'safe'],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoanApplication::className(), 'targetAttribute' => ['application_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'Application',
            'month_1' => 'Month 1',
            'month_2' => 'Month 2',
            'month_3' => 'Month 3',
            'month_4' => 'Month 4',
            'month_5' => 'Month 5',
            'month_6' => 'Month 6',
            'month_7' => 'Month 7',
            'month_8' => 'Month 8',
            'month_9' => 'Month 9',
            'month_10' => 'Month 10',
            'month_11' => 'Month 11',
            'month_12' => 'Month 12',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }

    /**
     * Gets query for [[Application]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(LoanApplication::className(), ['id' => 'application_id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
