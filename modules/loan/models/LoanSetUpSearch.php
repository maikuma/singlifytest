<?php

namespace app\modules\loan\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\loan\models\LoanSetUp;

/**
 * LoanSetUpSearch represents the model behind the search form of `app\modules\loan\models\LoanSetUp`.
 */
class LoanSetUpSearch extends LoanSetUp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'loan_interest', 'loan_processing_fee', 'loan_administrative_fee', 'loan_risk_fee', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['loan_name', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LoanSetUp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'loan_interest' => $this->loan_interest,
            'loan_processing_fee' => $this->loan_processing_fee,
            'loan_administrative_fee' => $this->loan_administrative_fee,
            'loan_risk_fee' => $this->loan_risk_fee,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'loan_name', $this->loan_name]);

        return $dataProvider;
    }
}
