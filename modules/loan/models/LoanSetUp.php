<?php

namespace app\modules\loan\models;

use app\models\User;
use app\Traits\CreatorTrait;
use Yii;

/**
 * This is the model class for table "loan_set_up".
 *
 * @property int $id
 * @property string $loan_name Name of the Loan
 * @property int|null $loan_interest The interest that will apply on the loan
 * @property int $loan_processing_fee The Process fee that will apply on the loan
 * @property int $loan_administrative_fee The Administrative fee that will apply on the loan
 * @property int $loan_risk_fee The Risk fee that will apply on the loan
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string|null $deleted_at
 * @property int $created_by
 * @property int $updated_by
 * @property int|null $deleted_by
 *
 * @property User $createdBy
 * @property LoanApplication[] $loanApplications
 * @property User $updatedBy
 */
class LoanSetUp extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    use CreatorTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan_set_up';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['loan_name', 'loan_processing_fee', 'loan_administrative_fee', 'loan_risk_fee', 'loan_interest'], 'required'],
            [['loan_interest', 'loan_processing_fee', 'loan_administrative_fee', 'loan_risk_fee', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'safe'],
            [['loan_name'], 'string', 'max' => 255],
            [['loan_name'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'loan_name' => 'Loan Name',
            'loan_interest' => 'Loan Interest %',
            'loan_processing_fee' => 'Loan Processing Fee %',
            'loan_administrative_fee' => 'Loan Administrative Fee %',
            'loan_risk_fee' => 'Loan Risk Fee %',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[LoanApplications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLoanApplications()
    {
        return $this->hasMany(LoanApplication::className(), ['loan_id' => 'id']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
