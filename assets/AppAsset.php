<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        //All Library CSS -->

        'css/bootstrap.min.css',
        'css/LineIcons.css',
        'css/viewer.min.css',
        'css/icofont.min.css',
        'css/calendar.css',
// Custom CSS
        'css/styles.css',
        'css/responsive.css',
    ];
    public $js = [
        //Optional JavaScript -->
        //jQuery first, then Popper.js, then Bootstrap JS -->
        'js/jquery.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
//Feather Icon JS -->
        'js/feather.min.js',
//Gllery viewer JS -->
        'js/viewer.min.js',

//ApexCharts JS -->
        'js/apex-charts/apexcharts.min.js',
        'js/apex-charts/apexcharts-stock-prices.js',
        'js/apex-charts/apex-line-charts.js',
        'js/apex-charts/apex-area-charts.js',
        'js/apex-charts/apex-bar-charts.js',
        'js/apex-charts/apex-mixed-charts.js',
        'js/apex-charts/apex-pie-donuts-charts.js',
        'js/apex-charts/sales-by-countries.js',
        'js/apex-charts/product-trends-by-month.js',
        'js/apex-charts/month-sales-statistics.js',
        'js/apex-charts/order-summary.js',
        'js/apex-charts/visitors-overview.js',
        'js/apex-charts/leads-stats.js',
        'js/apex-charts/apex-column-charts.js',
        'js/custom-chart.js',

//Custom JS -->
        'js/custom.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
