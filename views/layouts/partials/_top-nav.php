<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<nav class="navbar navbar-expand fixed-top top-menu">
    <a class="navbar-brand" href="/">
        <!-- Large logo -->
        <?php // Html::img('@web/images/large-logo.png', ['alt' => 'Logo', 'class' => 'large-logo']); ?>
        <!-- Small logo -->
        <?php // Html::img('@web/images/small-logo.png', ['alt' => 'Logo', 'class' => 'small-logo']); ?>
    </a>

    <!-- Burger menu -->
    <div class="burger-menu toggle-menu">
        <span class="top-bar"></span>
        <span class="middle-bar"></span>
        <span class="bottom-bar"></span>
    </div>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Mega Menu -->
        <ul class="left-nav d-none d-md-block navbar-nav">
            <li class="nav-item dropdown mega-menu">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <div class="mega-menu-btn">
                        Menu
                        <i data-feather="chevron-down" class="icon"></i>
                    </div>
                </a>

                <div class="dropdown-menu">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <h5 class="title">Set Up</h5>
                                <a class="dropdown-item" href="<?php echo Url::toRoute(['/loan/set-up/',]); ?>">Loan Product</a>
                                <a class="dropdown-item" href="<?php echo Url::toRoute(['/loan/customer/',]); ?>">Customer</a>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <h5 class="title">Loan</h5>
                                <a class="dropdown-item" href="<?php echo Url::toRoute(['/loan/application/',]); ?>">Application</a>
                            </div>

                        </div>
                    </div>
                </div>
            </li>
        </ul>

        <!-- Search form -->
<!--        <form class="nav-search-form d-none d-sm-block">-->
<!--            <input type="text" class="form-control" placeholder="Search...">-->
<!--            <button type="submit" class="search-success">-->
<!--                <i data-feather="search" class="icon"></i>-->
<!--            </button>-->
<!--        </form>-->

        <!-- Right nav -->
        <ul class="navbar-nav right-nav ml-auto">

            <!-- Profile dropdown -->
            <li class="nav-item dropdown profile-nav-item">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <div class="menu-profile">
                        <span class="name"><?= Html::encode(Yii::$app->user->identity->fullname) ?></span>
                        <?= Html::img('@web/images/user/1.jpg', ['alt' => 'Profile Image', 'class' => 'rounded-circle']); ?>
                    </div>
                </a>
                <div class="dropdown-menu">

                    <a class="dropdown-item" href="<?= \yii\helpers\Url::to(['/loan/set-up']) ?>">
                        <i data-feather="settings" class="icon"></i>
                        Loan Product
                    </a>
                    <a class="dropdown-item" href="<?= \yii\helpers\Url::to(['/site/logout']) ?>"
                           data-method="post">
                        <i data-feather="log-out" class="icon"></i>
                        Logout
                    </a>

                </div>
            </li>
        </ul>
    </div>
</nav>
