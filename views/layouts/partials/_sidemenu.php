<?php

use yii\helpers\Url;

?>
<div class="sidemenu-area sidemenu-toggle default">
    <nav class="sidemenu navbar navbar-expand navbar-light hide-nav-title">
        <div class="navbar-collapse collapse">
            <div class="navbar-nav">
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <div class="dropdown-title">
                            <i data-feather="grid" class="icon"></i>
                            <span class="title">
                                Dashboard
                                <i data-feather="chevron-right" class="icon fr"></i>
                            </span>
                        </div>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/">
                            <i data-feather="chevron-right" class="icon"></i>
                            Sales
                        </a>
                    </div>
                </div>

                <a class="nav-link" href="<?php echo Url::toRoute(['/loan/set-up/',]); ?>">
                    <i data-feather="check-square" class="icon"></i>
                    <span class="title">Set up Loan</span>
                </a>
                <a class="nav-link" href="<?php
                echo Url::toRoute(['/loan/customer/',]); ?>">
                    <i data-feather="check-square" class="icon"></i>
                    <span class="title">customer</span>
                </a>

                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <div class="dropdown-title">
                            <i data-feather="file-text" class="icon"></i>
                            <span class="title">
                                Loans
                                <i data-feather="chevron-right" class="icon fr"></i>
                            </span>
                        </div>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php
                        echo Url::toRoute(['/loan/application',]); ?>">
                            <i data-feather="chevron-right" class="icon"></i>
                            Loan Application
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
