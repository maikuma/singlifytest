<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

$this->title = 'Sign up';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Login Area -->
<div class="auth-main-content auth-bg-image">
    <div class="d-table">
        <div class="d-tablecell">
            <div class="auth-box">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="form-left-content">
                            <div class="auth-logo">
                                <?php // Html::img('@web/images/large-logo.png', ['alt' => 'Logo']); ?>
                            </div>

                            <div class="login-links">
                                <!--                                <a class="fb" href="#">-->
                                <!--                                    <i data-feather="facebook" class="icon"></i>-->
                                <!--                                    Sign Up with Facebook-->
                                <!--                                </a>-->
                                <!--                                <a class="twi" href="#">-->
                                <!--                                    <i data-feather="twitter" class="icon"></i>-->
                                <!--                                    Sign Up with Twitter-->
                                <!--                                </a>-->
                                <!--                                <a class="ema" href="#">-->
                                <!--                                    <i data-feather="mail" class="icon"></i>-->
                                <!--                                    Sign Up with Email-->
                                <!--                                </a>-->
                                <!--                                <a class="linkd" href="#">-->
                                <!--                                    <i data-feather="linkedin" class="icon"></i>-->
                                <!--                                    Sign Up with Linkedin-->
                                <!--                                </a>-->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-content">
                            <h1 class="heading">Sign up using</h1>
                            <?php $form = ActiveForm::begin([
                                //'id' => 'login-form',
                                //'class' => 'custom-form mt-4 pt-2',
                                'enableClientValidation' => true,
                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"form-group\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                    'labelOptions' => ['class' => 'form-label'],
                                ],
                            ]); ?>
                            <div class="form-group">
                                <label for="username" class="form-label">Full Name</label>
                                <?= $form->field($model, 'fullname', [
                                    'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'fullname']
                                ])->textInput()->input('text', ['placeholder' => "Enter Full Name *"])->label(false); ?>

                                <div class="invalid-feedback">
                                    <!--            Please Enter Full Name-->
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="form-label">Email address</label>
                                <!--        <input type="email" class="form-control" id="useremail" placeholder="Enter email" required>-->
                                <?= $form->field($model, 'email', [
                                    'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'email']
                                ])->textInput()->input('email', ['placeholder' => "Enter email *"])->label(false); ?>

                                <div class="invalid-feedback">
                                    <!--            Please Enter Email-->
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <!--        <input type="password" class="form-control" id="password" placeholder="Enter password" required>-->
                                <?= $form->field($model, 'password', [
                                    'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'password']
                                ])->textInput()->input('password', ['placeholder' => "Enter Password *"])->label(false); ?>
                                <div class="invalid-feedback">
                                    <!--            Please Enter Password-->
                                </div>
                            </div>

                            <div class="text-center">
                                <!--                                <button type="submit" class="btn btn-primary">Log In</button>-->
                                <?= Html::submitButton('Sign Up', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

                                <p class="text-muted mb-0">Already have an account ? <a href="<?php echo Url::toRoute(['/site/login']); ?>"
                                                                class="text-primary fw-semibold"> Login </a></p>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Login Area -->
