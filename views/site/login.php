<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Login Area -->
<div class="auth-main-content auth-bg-image">
    <div class="d-table">
        <div class="d-tablecell">
            <div class="auth-box">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="form-left-content">
                            <div class="auth-logo">
                                <?php // Html::img('@web/images/large-logo.png', ['alt' => 'Logo']); ?>
                            </div>

                            <div class="login-links">
                                <!--                                <a class="fb" href="#">-->
                                <!--                                    <i data-feather="facebook" class="icon"></i>-->
                                <!--                                    Sign Up with Facebook-->
                                <!--                                </a>-->
                                <!--                                <a class="twi" href="#">-->
                                <!--                                    <i data-feather="twitter" class="icon"></i>-->
                                <!--                                    Sign Up with Twitter-->
                                <!--                                </a>-->
                                <!--                                <a class="ema" href="#">-->
                                <!--                                    <i data-feather="mail" class="icon"></i>-->
                                <!--                                    Sign Up with Email-->
                                <!--                                </a>-->
                                <!--                                <a class="linkd" href="#">-->
                                <!--                                    <i data-feather="linkedin" class="icon"></i>-->
                                <!--                                    Sign Up with Linkedin-->
                                <!--                                </a>-->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-content">
                            <h1 class="heading">Log In</h1>
                            <?php $form = ActiveForm::begin([
                                //'id' => 'login-form',
                                //'class' => 'custom-form mt-4 pt-2',

                                'fieldConfig' => [
                                    'template' => "{label}\n<div class=\"form-group\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                    'labelOptions' => ['class' => 'form-label'],
                                ],
                            ]); ?>
                            <div class="form-group">
                                <label class="form-label">Email address</label>
                                <!--                                    <input type="email" class="form-control" id="email">-->

                                <?= $form->field($model, 'username', [
                                    'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'username']
                                ])->textInput()->input('email', ['placeholder' => "Enter username *"])->label(false); ?>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <!--                                    <input type="password" class="form-control" id="password">-->
                                <?= $form->field($model, 'password', [
                                    'inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'id' => 'password']
                                ])->textInput()->input('password', ['placeholder' => "Enter Password *"])->label(false); ?>
                            </div>
                            <div class="text-center">
                                <!--                                <button type="submit" class="btn btn-primary">Log In</button>-->
                                <?= Html::submitButton('Log In', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                <p class="text-muted mb-0">Don't have an account ? <a href="<?php echo Url::toRoute(['/site/signup']); ?>"
                                                                class="text-primary fw-semibold"> Sign up </a></p>
                                You may login with <strong>mukhwanak@gmail.com/123456</strong><br>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Login Area -->
